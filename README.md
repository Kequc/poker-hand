# PokerHand

![Cards](https://bytebucket.org/Kequc/poker-hand/raw/708ee2039bff186f84b4cd24bfb6ce5c967fcf54/images/cards.jpg)

Companion module for [deck.js](https://bitbucket.org/Kequc/deck).

### Usage

Include `poker-hand.js` into your page:
```html
<script src="poker-hand.js"></script>
```

Analyse a set of cards:
```javascript
var cards = [
    { rank: "10", suit: "clubs" },
    { rank: "9", suit: "hearts" },
    { rank: "8", suit: "hearts" },
    { rank: "7", suit: "diamonds" },
    { rank: "jack", suit: "clubs" }
];
var hand = PokerHand.score(cards);
#=> {
    cards: [
        { rank: "10", suit: "clubs" },
        { rank: "9", suit: "hearts" },
        { rank: "8", suit: "hearts" },
        { rank: "7", suit: "diamonds" },
        { rank: "jack", suit: "clubs" }
    ],
    name: "straight",
    value: 40908070605
}
```

Add as many card sets together as you like:
```javascript
var hand = PokerHand.score(cards, [
    { rank: "queen", suit: "clubs" },
    { rank: "king", suit: "clubs" },
    { rank: "ace", suit: "clubs" }
]);
#=> {
    cards: [
        { rank: "10", suit: "clubs" },
        { rank: "jack", suit: "clubs" },
        { rank: "queen", suit: "clubs" },
        { rank: "king", suit: "clubs" },
        { rank: "ace", suit: "clubs" }
    ],
    name: "royal flush",
    value: 91211100908
}
```

### Result

What the result means:
```
cards:
These are the cards which were valid to make the hand.

name:
royal flush
straight flush
four of a kind
full house
flush
straight
three of a kind
two pair
one pair
high card
nothing

value:
A number which can be compared against other hands to find the winner.
```

### Limits

* Cards which are to be analysed are expected have both rank and suit.
* Cards must have a valid poker rank.
```
2 3 4 5 6 7 8 9 10 jack queen king ace
```
* Must provide at least 5 cards in order to return a hand with expected value greater than 0.
