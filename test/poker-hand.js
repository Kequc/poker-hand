"use strict";
var mocha = require('mocha');
var expect = require('chai').expect;
var assert = require('chai').assert;
var PokerHand = require('../lib/poker-hand');

describe('hand.js', () =>
{
    let _cards = {
        'ah': { rank: 'ace', suit: 'hearts' },
        'ad': { rank: 'ace', suit: 'diamonds' },
        'kh': { rank: 'king', suit: 'hearts' },
        'kd': { rank: 'king', suit: 'diamonds' },
        'qh': { rank: 'queen', suit: 'hearts' },
        'qd': { rank: 'queen', suit: 'diamonds' },
        'jh': { rank: 'jack', suit: 'hearts' },
        'jd': { rank: 'jack', suit: 'diamonds' },
        'th': { rank: '10', suit: 'hearts' },
        'td': { rank: '10', suit: 'diamonds' },
        '9h': { rank: '9', suit: 'hearts' },
        '9d': { rank: '9', suit: 'diamonds' },
        '9c': { rank: '9', suit: 'clubs' },
        '9s': { rank: '9', suit: 'spades' },
        '8d': { rank: '8', suit: 'diamonds' },
        '7h': { rank: '7', suit: 'hearts' },
        '7d': { rank: '7', suit: 'diamonds' },
        '7c': { rank: '7', suit: 'clubs' },
        '7s': { rank: '7', suit: 'spades' },
        '6d': { rank: '6', suit: 'diamonds' },
        '5h': { rank: '5', suit: 'hearts' },
        '5d': { rank: '5', suit: 'diamonds' },
        '5c': { rank: '5', suit: 'clubs' },
        '5s': { rank: '5', suit: 'spades' },
        '4s': { rank: '4', suit: 'spades' },
        '3h': { rank: '3', suit: 'hearts' },
        '3d': { rank: '3', suit: 'diamonds' },
        '2h': { rank: '2', suit: 'hearts'}
    };
    
    describe('common hands', () =>
    {
        let _sets = {
            'royal flush': [
                [_cards['ah'], _cards['kh'], _cards['qh'], _cards['jh'], _cards['th']],
                [_cards['td'], _cards['ad'], _cards['kd'], _cards['qd'], _cards['jd']]
            ],
            'straight flush': [
                [_cards['kh'], _cards['qh'], _cards['jh'], _cards['th'], _cards['9h']],
                [_cards['td'], _cards['7d'], _cards['9d'], _cards['8d'], _cards['6d']]
            ],
            'four of a kind': [
                [_cards['7h'], _cards['7d'], _cards['7c'], _cards['7s'], _cards['9s']],
                [_cards['5h'], _cards['5s'], _cards['9s'], _cards['5d'], _cards['5c']]
            ],
            'full house': [
                [_cards['7h'], _cards['7d'], _cards['7c'], _cards['9d'], _cards['9s']],
                [_cards['kh'], _cards['9h'], _cards['9c'], _cards['kd'], _cards['9d']]
            ],
            'flush': [
                [_cards['ah'], _cards['qh'], _cards['th'], _cards['7h'], _cards['5h']],
                [_cards['6d'], _cards['kd'], _cards['9d'], _cards['8d'], _cards['5d']]
            ],
            'straight': [
                [_cards['kd'], _cards['qh'], _cards['jh'], _cards['th'], _cards['9d']],
                [_cards['5d'], _cards['6d'], _cards['9c'], _cards['7s'], _cards['8d']],
                [_cards['ah'], _cards['2h'], _cards['3h'], _cards['4s'], _cards['5s']]
            ],
            'three of a kind': [
                [_cards['7h'], _cards['7d'], _cards['7c'], _cards['ah'], _cards['5h']],
                [_cards['9h'], _cards['9s'], _cards['9d'], _cards['5s'], _cards['kd']]
            ],
            'two pair': [
                [_cards['7h'], _cards['7d'], _cards['5h'], _cards['5c'], _cards['ah']],
                [_cards['ah'], _cards['5s'], _cards['ad'], _cards['9d'], _cards['9s']]
            ],
            'one pair': [
                [_cards['5h'], _cards['5c'], _cards['9h'], _cards['kd'], _cards['ah']],
                [_cards['6d'], _cards['qd'], _cards['5c'], _cards['qh'], _cards['ah']]
            ],
            'high card': [
                [_cards['kh'], _cards['jh'], _cards['9d'], _cards['7c'], _cards['5c']],
                [_cards['jh'], _cards['qd'], _cards['7h'], _cards['5s'], _cards['9d']]
            ]
        };
        
        it('detects each', () =>
        {
            for (let name of Object.keys(_sets)) {
                for (let i = 0; i < _sets[name].length; i++) {
                    let hand = PokerHand.score(_sets[name][i]);
                    expect(hand.name).to.equal(name);
                }
            }
        });
        
        it('detects none', () =>
        {
            let hand = PokerHand.score([_cards['5h'], _cards['5c']]);
            expect(hand.name).to.equal('nothing');
            expect(hand.value).to.equal(0);
        });
        
        it('concatonates card sets', () =>
        {
            let c = _sets['full house'][0];
            let hand;
            hand = PokerHand.score(c, [_cards['5s'], _cards['7s']]);
            expect(hand.name).to.equal('four of a kind');
            hand = PokerHand.score(c, [_cards['7s'], _cards['5s']]);
            expect(hand.name).to.equal('four of a kind');
            hand = PokerHand.score(c, [_cards['5s']], [_cards['7s']]);
            expect(hand.name).to.equal('four of a kind');
            hand = PokerHand.score(c, [_cards['7s']], [_cards['5s']]);
            expect(hand.name).to.equal('four of a kind');
        });
        
        describe('winning scenario', () =>
        {
            function winner (win, lose) {
                expect(PokerHand.score(win).value).to.be.above(PokerHand.score(lose).value);
            }

            it('high card king beats high card queen', () => {
                winner(_sets['high card'][0], _sets['high card'][1]);
            });

            it('four of a kind high card ace beats four of a kind high card nine', () => {
                let c = _sets['four of a kind'][0];
                winner(c.concat([_cards['ah'], _cards['5s']]), c.concat([_cards['5h'], _cards['5d']]));
            });

            it('four of a kind high card ten beats four of a kind high card five', () => {
                let c = _sets['four of a kind'][1];
                winner(c.concat([_cards['th'], _cards['3h']]), c.concat([_cards['3h'], _cards['3d']]));
            });

            it('two pair beats one pair', () => {
                winner(_sets['two pair'][0], _sets['one pair'][0]);
            });

            it('low straight beats wheel', () => {
                winner(_sets['straight'][0], _sets['straight'][2]);
                winner(_sets['straight'][1], _sets['straight'][2]);
            });
        });
        
    });
});
